#include <ctime>
#include <iostream>
#include <Windows.h>

CRITICAL_SECTION sticks[5];



DWORD WINAPI eating(LPVOID param)
{
	int rightIndex = 0;
	int leftIndex = 0;

	//this part checks for the person which sticks should he check
	if (*(int *)param == 1)
		rightIndex = 4;
	else
		rightIndex = *(int*)param - 1;

	if (*(int *)param == 5)
		leftIndex = 0;
	else
		leftIndex = *(int*)param;


	for (int i = 0; i < 1000000; i++)
	{
		EnterCriticalSection(&sticks[rightIndex]);  //waits to catch the right stick
		if (TryEnterCriticalSection(&sticks[leftIndex]))   //tries to catch the left sticks - if he doesn't catch it he will live the right stick (he doesn't wait for the left stick)
		{
			LeaveCriticalSection(&sticks[leftIndex]);
		}
		else
			i--;
		LeaveCriticalSection(&sticks[rightIndex]);
	}
	std::cout << "DONE!";
	return 0;
}

int main()
{
	int indexes[5] = { 1, 2, 3, 4, 5 };
	const clock_t begin_time = clock();
	HANDLE WINAPI hThread[5];
	for (int i = 0; i < 5; i++)
		InitializeCriticalSection(&sticks[i]);
	for (int i = 0; i < 5; i++)
	{
		hThread[i] = CreateThread(
			NULL,
			0,
			eating,
			&indexes[i],
			0,
			NULL);
	}

	WaitForSingleObject(hThread[0], INFINITE);
	WaitForSingleObject(hThread[1], INFINITE);
	WaitForSingleObject(hThread[2], INFINITE);
	WaitForSingleObject(hThread[3], INFINITE);
	WaitForSingleObject(hThread[4], INFINITE);

	std::cout << float(clock() - begin_time) / CLOCKS_PER_SEC << std::endl;

	system("pause");
	return 0;
}
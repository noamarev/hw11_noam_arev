#include <windows.h>
#include <ctime>
#include <stdio.h>
#include <tchar.h>
#include <string>
#include <iostream>


int main(int argc, TCHAR* argv[])
{
	if (argc > 0)
	{
		if (argv[1] == NULL)  //if this is the main process that creates the other processes
		{
			bool processOpened[5];
			const clock_t begin_time = clock();
			STARTUPINFO si[5];
			PROCESS_INFORMATION pi[5];

			//creates all 5 mutexes
			for (int i = 0; i < 5; i++)
				CreateMutex(NULL, false, std::to_string(i).c_str());

			for (int i = 0; i < 5; i++)
			{
				ZeroMemory(&(si[i]), sizeof(si[i]));
				si[i].cb = sizeof(si[i]);
				ZeroMemory(&(pi[i]), sizeof(pi[i]));
			}

			//opens processes
			for (int i = 0; i < 5; i++)
			{
				std::string s = "mutex.exe " + std::to_string(i+1);
				char process[12] = { 0 };
				for (int j = 0; j < 11; j++)
					process[j] = s[j];
				processOpened[i] = CreateProcessA(NULL, process, NULL, NULL, FALSE, 0, NULL, NULL, &(si[i]), &(pi[i]));
			}
			WaitForSingleObject(pi[0].hProcess, INFINITE);
			WaitForSingleObject(pi[1].hProcess, INFINITE);
			WaitForSingleObject(pi[2].hProcess, INFINITE);
			WaitForSingleObject(pi[3].hProcess, INFINITE);
			WaitForSingleObject(pi[4].hProcess, INFINITE);
			std::cout << float(clock() - begin_time) / CLOCKS_PER_SEC << std::endl;
			for (int i = 0; i < 5; i++)
			{
				CloseHandle(si[i].hStdInput);
				CloseHandle(si[i].hStdOutput);
				CloseHandle(si[i].hStdError);
				CloseHandle(pi[i].hProcess);
				CloseHandle(pi[i].hThread);
			}
			system("pause");
				
		}
		else
		{
			std::string::size_type sz;
			HANDLE rightMutex;
			HANDLE leftMutex;

			//checks for each process which stick (mutex) should he check
			if (!strcmp(argv[1], "1"))
				rightMutex = CreateMutex(NULL, false, "4");
			else
				rightMutex = CreateMutex(NULL, false, std::to_string((std::stoi(argv[1], &sz) - 1)).c_str());;

			if (!strcmp(argv[1], "5"))
				leftMutex = CreateMutex(NULL, false, "0");
			else
				leftMutex = CreateMutex(NULL, false, argv[1]);

			for (int i = 0; i < 1000000; i++)
			{
				WaitForSingleObject(rightMutex, INFINITE);  //waits to catch the right stick
				if (WaitForSingleObject(leftMutex, 0) == !WAIT_ABANDONED)   //tries to catch the left sticks - if he doesn't catch it he will live the right stick (he doesn't wait for the left stick)
				{
					ReleaseMutex(leftMutex);
				}
				ReleaseMutex(rightMutex);

			}
			std::cout << "DONE!";
		}
	}
	
	return 0;
}